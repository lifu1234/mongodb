package com.fengyulv.fyl.system.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "index")
public class indexPo {

    /**
     * 排序字段
     */
    private Long sort;

    @Id
    private String id;

    /**
     * 用户名
     */
    @NotEmpty(message = "用户名不能为空")
    private String username;
    /**
     * 密码
     */
    @NotEmpty(message = "密码不能为空")
    private String pwd;
    /**
     * 年龄
     */
    @NotEmpty(message = "年龄不能为空")
    private String age;
    /**
     * 职位
     */
    @NotEmpty(message = "职位不能为空")
    private String position;

}
