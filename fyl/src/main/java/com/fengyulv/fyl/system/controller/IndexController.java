package com.fengyulv.fyl.system.controller;

import com.fengyulv.fyl.common.config.Result;
import com.fengyulv.fyl.common.untils.MongodbUtils;
import com.fengyulv.fyl.common.untils.RedisUtils;
import com.fengyulv.fyl.system.po.indexPo;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/index")
public class IndexController {

    @Resource
    private MongodbUtils mongodbUtils;

    @Resource
    private MongoTemplate mongoTemplate;

    @Resource
    private RedisUtils redisUtils;

    //注意：@controller不是@RestController，使用@RestController会返回“index”字符串
    @GetMapping("/index")
    public String index() {
        return "index";
    }

    /**
     * 新增文档
     */
    @PostMapping("/addIndex")
    @ResponseBody
    public Result addIndex(@Valid indexPo indexPo) {
        try {
            //自增
            long sort = this.redisUtils.incr("sort", 1L);
            indexPo.setSort(sort);
            this.mongodbUtils.saveData(indexPo, "index");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(500, "保存失败！");
        }
        return Result.ok();
    }

    /**
     * 删除文档
     *
     * @param documentId
     * @return
     */
    @PostMapping("/deleteIndexById")
    @ResponseBody
    public Result deleteIndexById(@RequestParam(value = "documentId[]") String[] documentId) {
        try {
            //删除
            this.mongodbUtils.updateDataPLIsDel(documentId, indexPo.class);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(500, "删除失败，服务器异常！");
        }
        return Result.ok();
    }

    /**
     * 更新文档
     *
     * @param indexPo
     * @return
     */
    @PostMapping("/updateIndex")
    @ResponseBody
    public Result updateIndex(@Valid indexPo indexPo) {
        try {
            //删除
            this.mongodbUtils.removeById(indexPo.getId(), indexPo.class);
            //增加
            this.mongodbUtils.saveData(indexPo, "index");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(500, "修改失败，服务器异常！");
        }
        return Result.ok();
    }

    /**
     * 分页获取全部属性
     *
     * @return Result
     */
    @PostMapping("/getIndexDataByPage")
    @ResponseBody
    public Result getIndexDataByPage(@RequestParam(value =
            "page", required = false) Integer page, @RequestParam(value = "limit", required = false) Integer limit) {
        try {
            //获取所有标志的信息
            if (page == null || limit == null) {
                List<indexPo> all = this.mongoTemplate.find(Query.query(Criteria.where("_id").exists(true)), indexPo.class);
                return Result.ok().put("data", all);
            } else {
                //分页获取所有的信息
                Map<String, Object> allByPage = this.mongodbUtils.findAllByPage(indexPo.class, page, limit);
                Integer count = Integer.valueOf(allByPage.get("count").toString());
                List<indexPo> resultList = (List<indexPo>) allByPage.get("data");
                return Result.ok().put("data", resultList).put("count", count);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error(500, "数据库异常！");
        }
    }

    /**
     * 按条件分页获取信息
     *
     * @param page
     * @param limit
     * @param keys
     * @param values
     * @return
     */
    @PostMapping("/getAllIndexDataByKeyWord")
    @ResponseBody
    public Result getAllIndexDataByKeyWord(Integer page, Integer limit, @RequestParam("keys[]") String[] keys, @RequestParam("values[]") String[] values) {
        Integer count = 0;
        List<indexPo> collect = new ArrayList<>();
        //没条件就直接返回
        if (keys.length < 1) {
            return getIndexDataByPage(page, limit);
        } else {
            //获取所有满足条件的结果
            Map<String, Object> dataByKey = this.mongodbUtils.findDataByKey(indexPo.class, keys, values, page, limit);
            count = Integer.valueOf(dataByKey.get("count").toString());
            collect = (List<indexPo>) dataByKey.get("data");
        }
        return Result.ok().put("data", collect).put("count", count);
    }


}
