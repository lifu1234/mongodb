package com.fengyulv.fyl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.fengyulv.fyl")
public class FylApplication {
    public static void main(String[] args) {
        SpringApplication.run(FylApplication.class, args);
    }
}
